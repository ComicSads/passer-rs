use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::level;
use crate::player;
use crate::player::PlayerColor;

pub struct WallPlugin;

/// Size in pixels(?) of wall
pub const SIZE: f32 = player::SIZE;

#[derive(Component)]
struct Wall {
	color: WallColor,
}

impl Plugin for WallPlugin {
	fn build(&self, app: &mut App) {
		app.add_systems(Startup, setup_wall)
			.add_systems(OnEnter(PlayerColor::White), disable_collision_white)
			.add_systems(OnEnter(PlayerColor::Black), disable_collision_black)
			.add_systems(OnExit(PlayerColor::White), enable_collision_white)
			.add_systems(OnExit(PlayerColor::Black), enable_collision_black);
	}
}

macro_rules! collision_setup {
	($disable_name:ident, $enable_name:ident, $wall_color:expr) => {
		fn $disable_name(mut commands: Commands, query: Query<(Entity, &Wall)>) {
			for (entity, _wall) in query.iter().filter(|(_, w)| w.color == $wall_color) {
				commands.entity(entity).insert(ColliderDisabled);
			}
		}

		fn $enable_name(mut commands: Commands, query: Query<(Entity, &Wall)>) {
			for (entity, _wall) in query.iter().filter(|(_, w)| w.color == $wall_color) {
				commands.entity(entity).remove::<ColliderDisabled>();
			}
		}
	};
}

// We use opposite colors because it means you can see the player when phasing through a block
collision_setup!(
	disable_collision_white,
	enable_collision_white,
	WallColor::Black
);
collision_setup!(
	disable_collision_black,
	enable_collision_black,
	WallColor::White
);

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum WallColor {
	White,
	Black,
}

impl WallColor {
	const fn as_bevy_color(self) -> Color {
		match self {
			Self::White => Color::WHITE,
			Self::Black => Color::BLACK,
		}
	}

	pub fn build(s: &str) -> Self {
		match s {
			"Black" => Self::Black,
			"White" => Self::White,
			_ => unreachable!("WallColor was built with a weird string"),
		}
	}
}

impl From<player::PlayerColor> for WallColor {
	fn from(value: player::PlayerColor) -> Self {
		match value {
			player::PlayerColor::White => Self::White,
			player::PlayerColor::Black => Self::Black,
		}
	}
}

fn setup_wall(mut commands: Commands, asset_server: Res<AssetServer>) {
	let texture = asset_server.load("sprites/Wall.png");

	let list_of_walls = level::get_wall_info("levels/level1.ron");

	for wall_info in list_of_walls {
		commands.spawn((
			SpriteBundle {
				sprite: Sprite {
					color: wall_info.color.as_bevy_color(),
					custom_size: Some(Vec2::new(SIZE, SIZE)),
					..default()
				},
				texture: texture.clone(),
				transform: Transform {
					translation: Vec3::from((wall_info.pos, 0.0)),
					..default()
				},
				..default()
			},
			Collider::cuboid(SIZE / 2.0, SIZE / 2.0),
			Wall {
				color: wall_info.color,
			},
		));
	}
}
