#![allow(dead_code)]
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

mod level;
mod player;
mod wall;

const GRAVITY: f32 = -9.81 * 10.0;

fn main() {
	App::new()
		.add_plugins(
			DefaultPlugins
				.set(ImagePlugin::default_nearest())
				.set(WindowPlugin {
					primary_window: Some(Window {
						title: "Passer".into(),
						resolution: (640.0, 480.0).into(),
						resizable: false, //TODO: why
						..default()
					}),
					..default()
				}),
		)
		.add_plugins((
			RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(100.0),
			RapierDebugRenderPlugin::default(),
		))
		.add_plugins(player::PlayerPlugin)
		.add_plugins(wall::WallPlugin)
		.add_systems(Startup, setup_game)
		.add_systems(Update, bevy::window::close_on_esc)
		.run();
}

fn setup_game(mut commands: Commands, mut rapier_config: ResMut<RapierConfiguration>) {
	rapier_config.gravity = Vec2::new(0.0, GRAVITY);
	commands.spawn(Camera2dBundle::default());
}
