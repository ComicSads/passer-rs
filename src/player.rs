use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

pub const SIZE: f32 = 100.0;

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
	fn build(&self, app: &mut App) {
		app.add_systems(Startup, setup_player)
			.add_state::<PlayerColor>()
			.add_systems(Update, player_movement)
			.add_systems(OnEnter(PlayerColor::White), color_change_white)
			.add_systems(OnEnter(PlayerColor::Black), color_change_black)
			.add_systems(Update, player_color_state);
	}
}

#[derive(Component)]
struct Player {
	speed: f32,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug, Default, States)]
pub enum PlayerColor {
	White,
	#[default]
	Black,
}

impl PlayerColor {
	const fn opposite(self) -> Self {
		match self {
			Self::White => Self::Black,
			Self::Black => Self::White,
		}
	}

	const fn as_bevy_color(self) -> Color {
		match self {
			Self::White => Color::WHITE,
			Self::Black => Color::BLACK,
		}
	}
}

fn player_movement(
	mut characters: Query<(&mut KinematicCharacterController, &Player)>,
	input: Res<Input<KeyCode>>,
	time: Res<Time>,
) {
	let (mut player_controller, player) = characters.single_mut();
	let move_speed = player.speed * time.delta_seconds();

	let mut translation = Vec2::ZERO;

	if input.pressed(KeyCode::D) {
		translation.x += move_speed;
	}

	if input.pressed(KeyCode::A) {
		translation.x += move_speed * -1.0;
	}

	player_controller.translation = Some(translation);
}

fn player_color_state(
	color: Res<State<PlayerColor>>,
	mut next_color: ResMut<NextState<PlayerColor>>,
	input: Res<Input<KeyCode>>,
) {
	if input.just_pressed(KeyCode::E) {
		let new_color = color.opposite();
		let writeable = next_color.as_mut();
		writeable.0 = Some(new_color);
	}
}

fn color_change_white(mut sprites: Query<&mut Sprite, With<Player>>) {
	for mut sprite in &mut sprites {
		sprite.color = Color::WHITE;
	}
}

fn color_change_black(mut sprites: Query<&mut Sprite, With<Player>>) {
	for mut sprite in &mut sprites {
		sprite.color = Color::BLACK;
	}
}

fn setup_player(mut commands: Commands, asset_server: Res<AssetServer>) {
	let texture = asset_server.load("sprites/Player.png");

	let sprite_size = SIZE;
	let half_sprite_size: f32 = sprite_size / 2.0;

	let player_point_a = Vect::new(-half_sprite_size, half_sprite_size);
	let player_point_b = Vect::new(-half_sprite_size, -half_sprite_size);
	let player_point_c = Vect::new(half_sprite_size, -half_sprite_size);

	commands.spawn((
		SpriteBundle {
			sprite: Sprite {
				custom_size: Some(Vec2::new(sprite_size, sprite_size)),
				color: Color::BLACK,
				..default()
			},
			transform: Transform {
				translation: Vec3::new(0.0, 200.0, 1.0),
				..default()
			},
			texture,
			..default()
		},
		RigidBody::Dynamic,
		LockedAxes::ROTATION_LOCKED,
		Velocity::zero(),
		Collider::triangle(player_point_a, player_point_b, player_point_c),
		Player { speed: 100.0 },
		KinematicCharacterController::default(),
	));
}
