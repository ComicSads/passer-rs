use bevy::prelude::*;
use ron::de::from_reader;
use serde::Deserialize;
use std::fs::File;

use crate::wall;
use crate::wall::WallColor;

pub struct WallSpawnInfo {
	pub pos: Vec2,
	pub color: WallColor,
}

#[derive(Debug, Deserialize)]
struct Level {
	vec: Vec<RawWallSpawnInfo>,
}

#[derive(Debug, Deserialize)]
struct RawWallSpawnInfo {
	x: i32,
	y: i32,
	color: String,
}

impl RawWallSpawnInfo {
	fn as_wall_spawn_info(&self) -> WallSpawnInfo {
		let tile_x: f32 = self.x as f32;
		let tile_y: f32 = self.y as f32;
		let pos = Vec2::new(tile_x * wall::SIZE, tile_y * wall::SIZE);
		let color: WallColor = WallColor::build(&self.color);
		WallSpawnInfo { pos, color }
	}
}

pub fn get_wall_info(level: &str) -> Vec<WallSpawnInfo> {
	// use WallColor as WC;
	let level = level;

	let f = File::open(level).expect("Failed to open level file");
	let level: Level = match from_reader(f) {
		Ok(x) => x,
		Err(e) => panic!("Error reading level file {e}"),
	};

	level.vec.iter().map(|x| x.as_wall_spawn_info()).collect()
}
